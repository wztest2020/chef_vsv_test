Test role for install and configure apache
===============
Cookbook - apache-vsv-test  
Role - test_role

Attributes  
-------------------
doc_root - path to folder where to store domain content [default: /mnt/web]  
domain - new domain name [default: example.com]  

