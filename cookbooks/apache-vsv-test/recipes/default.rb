#
# Cookbook:: apache-vsv-test
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.
package 'apache2'

service 'apache2' do
  supports :status => true
  action [:enable, :start]
end

directory "#{node['apache2']['doc_root']}/#{node['apache2']['domain']}" do
#  owner 'www-data'
#  group 'www-data'
  recursive true
  mode '0755'
  action :create
end

template "/etc/apache2/sites-available/#{node['apache2']['domain']}.conf" do
  source "vhost.erb"
  variables({ :doc_root => node['apache2']['doc_root'],
              :domain => node['apache2']['domain'] })
  action :create
  notifies :reload, resources(:service => "apache2")
end

link "/etc/apache2/sites-enabled/#{node['apache2']['domain']}.conf" do
  to "/etc/apache2/sites-available/#{node['apache2']['domain']}.conf" 
end

template "#{node['apache2']['doc_root']}/#{node['apache2']['domain']}/index.html" do
  source 'index.html.erb'
  variables({ :text => node['apache2']['domain'] })
  action :create
end
