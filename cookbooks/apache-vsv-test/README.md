# apache-vsv-test
Test cookbook to install and add domain.

# Attributes
doc_root - path to folder where to store domain content [default: /mnt/web]
domain - new domain name [default: example.com]
