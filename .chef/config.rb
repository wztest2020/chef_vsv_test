# See https://docs.getchef.com/config_rb.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "admin_test"
client_key               "#{current_dir}/admin_test.pem"
chef_server_url          "https://chef-test-server/organizations/test"
cookbook_path            ["#{current_dir}/../cookbooks"]
knife[:editor] = 'vim'
